import React from 'react'

type HeadingProps = { name?: string }

export const Heading = ({ name = 'React' }: HeadingProps) => {
  return <h1 className='x'>Hello {name}</h1>
}
