import React from 'react'
import { render } from '@testing-library/react'
import { Heading } from './Heading'

test('renders heading', () => {
  const { getByText } = render(<Heading />)
  const linkElement = getByText(/Hello react/i)
  expect(linkElement).toBeInTheDocument()
})

test('renders header with argument', () => {
  const { getByText } = render(<Heading name='Hello react' />)
  const linkElement = getByText(/Hello react/i)
  expect(linkElement).toBeInTheDocument()
})
